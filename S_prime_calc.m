function [ S_prime ] = S_prime_calc( s )
% S_prime_calc: quick calculation of the spectrogram (S_prime) based on the current E_xuv and A vectors 

    W_low = s.W_range(1);
    W_high = s.W_range(end);
    S_prime = zeros(size(s.k_vec, 2), size(s.tau_range, 2));
    
    for l=1:size(s.k_vec, 2)
 
       % check if dipole is included: DIPOLE is always included
      if ((any(any((s.k_vec(l) + s.A))) < sqrt(2*W_low)) && (any(any((s.k_vec(l) + s.A))) > sqrt(2*W_high)))
          f_real = 0;
          f_imag = 0;
      else
          %interpolate the correct point
          X = ((s.k_vec(l)+s.A).^2)/2;
          if(any(X < s.W_dipole(1)) || any(X > s.W_dipole(end)))
              %falling outside the range -> linear extrapolation
              f_real = interp1(s.W_dipole, s.real_dk, X, 'linear', 'extrap');
              f_imag = interp1(s.W_dipole, s.imag_dk, X, 'linear', 'extrap');
          else
              %no extrapolation needed -> spline
              f_real = interp1(s.W_dipole, s.real_dk, X, 'spline');
              f_imag = interp1(s.W_dipole, s.imag_dk, X, 'spline');
          end
      end

       % load the table
       CV = (f_real + 1i*(f_imag)).*exp(-1i*(s.action_matrix(l,:)));
       %perform integral for S_prime calculation at each momentum and delay
       for m=1:size(s.tau_range, 2)
           S_prime(l,m) = -1i*s.del_t*sum(s.E_XUV.*exp(1i*(s.W_range(l) - (s.k0^2)/2)*s.t_XUV).*CV(s.L*(m-1) + 1 : s.N_P+s.L*(m-1)));
       end
    end
end

