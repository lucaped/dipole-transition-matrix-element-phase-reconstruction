# Dipole transition matrix element phase reconstruction 
Authors: *L Pedrelli, P D Keathley, L Cattaneo, F X Kärtner, and U Keller* \
Email: lucaped@phys.ethz.ch

### Abstract
Coherent, broadband pulses of extreme ultraviolet light provide a new and exciting tool for exploring attosecond electron dynamics. Using  photoelectron streaking, interferometric spectrograms can be generated that contain a wealth of information about the phase properties of the photoionization process. If properly retrieved, this phase information reveals attosecond dynamics during photoelectron emission such as multielectron dynamics and resonance processes.\
However, until now, the full retrieval of the continuous electron wavepacket phase from isolated attosecond pulses has remained challenging.\
Here, after elucidating key approximations and limitations that hinder one from extracting the coherent electron wavepacket dynamics using available retrieval algorithms, we present a new method called **absolute complex dipole transition matrix element reconstruction (ACDC)**. We apply the ACDC method to experimental spectrograms to resolve the phase and group delay difference between photoelectrons emitted from Ne and Ar. Our results reveal subtle dynamics in this group delay difference of photoelectrons emitted form Ar.\
These group delay dynamics were not resolvable with prior methods that were only able to extract phase information at discrete energy levels, emphasizing the importance of a complete and continuous phase retrieval technique such as ACDC. Here we also make this new ACDC retrieval algorithm available with appropriate citation in return.

<div align="center">
    <figure>
        <img src="Images/Figure1.png"  width="85%" height="85%" class="center">
        <font size="2">
        <figcaption> <strong>Figure 1) </strong> (a) Scheme of the experimental setup including the COLTRIMS detector used to simultaneously measure the streaking spectrograms of Ne (b) and Ar (c) considered in this work. The red line represents the path of the IR beam which is spited in two parts by the beam splitter (BS). 80% is used to generate, using the polarization gating (PG) scheme, the XUV pump beam (E<sub>XUV</sub>) while the remaining 20% is used as the probe streaking field (E<sub>IR</sub>). The scheme of the experimental setup has been adapted from reference [1]. 
        </figcaption>
        </font>
    </figure>
    <figure>
        <img src="Images/Figure2.png"  width="50%" height="50%" class="center">
        <font size="2">
        <figcaption> <strong>Figure 2) </strong> The blue curve displays the average of 36 retrieved Ar dipole phases from the reconstruction of experimental measurements using the ACDC algorithm. The yellow symbols are adapted experimental result from reference [2] and shifted to
within a constant offset. With the ACDC algorithm we could increase the resolution of the retrieved Ar dipole phase of a factor 20. 
        </figcaption>
        </font>
    </figure>
</div>

References:\
[1] Sabbar M, _et al_ 2014 Combining attosecond XUV pulses with coincidence spectroscopy _Rev. Sci. Instrum._ **85** 103113\
[2] Azoury D, _et al_ 2019 Electronic wavefunctions probed by all-optical attosecond interferometry _Nat. Photon._ **13** 54–9

# Reference
Here we provide the code which has been described in details in the following publication: \
Title: **Complete phase retrieval of photoelectron wavepackets** \
Authors: Luca Pedrelli, Phillip D Keathley, Laura Cattaneo, Franz X Kaertner and Ursula Keller \
Link: https://doi.org/10.1088/1367-2630/ab83d7 \
See licensing terms for details.

# Contents
| Script | Description |
| ------ | ------ |
| main.m | main script |
| S\_prime\_calc.m | function to calculate the reconstructed spectrogram | 
| calculate\_error.m | function to compute the error | 

| Folder | Description |
| ------ | ------ |
| Example input files | contains example input files to run the reconstruction algorithm |
| Images | relevant images |

# Disclaimer
All functions in this toolbox were implemented with care and tested.\
Nevertheless, they may contain errors or bugs, which may affect the outcome of your analysis.\
We do not take responsibility for any harm coming from using this toolbox, 
neither if it is caused by errors in the software nor if it is caused by its improper application.\
See licensing terms for details. \
Please email us any bugs you find.