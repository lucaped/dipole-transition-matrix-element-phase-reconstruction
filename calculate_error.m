function [ error ] = calculate_error( S_range, S_prime, delta_W_vec, delta_W_vec_sum )
% calculate error between S_range and S_prime

    error = 0;
    for i=1:size(S_range, 1)
        for j=1:size(S_range, 2)
            error = error + ((abs(abs(S_prime(i,j))-sqrt(S_range(i,j))))^2)*delta_W_vec(i);
        end
    end
    error = error/(size(S_range, 2)*delta_W_vec_sum);
end

