% -------------------------------------------------------------------------------%
% Authors: Luca Pedrelli, Phillip D Keathley, Laura Cattaneo, Franz X Kaertner and Ursula Keller
% Link to the original paper: https://iopscience.iop.org/article/10.1088/1367-2630/ab83d7
% Email: lucaped@phys.ethz.ch
% Created: 19/03/2020 
% Last update: 02/04/2020
% -------------------------------------------------------------------------------%
clc; clear all; close all

% -----------------------------------------------------------------------%
% --------- CONSTANTS - do not change them ------------------------------%
% -----------------------------------------------------------------------%
tcon = 1/(2.418884326505e-17); %atomic time/second
evcon = 1/27.211; % conversion factor J_a/eV
lcon = 1/(5.291772192e-11); %atomic length/meter
c = 2.99792458*2.418884326505*1e2/5.291772192; %speed of light [m_a/s_a]
efieldcon = 1/(5.14220652e11); %atomic E-field/(V/m)
h = 6.62606957e-34; %J.s
el = 1.602176565e-19;
h_bar=h/(2*pi); %J.s = 1.0545718e-34 J.s
% -----------------------------------------------------------------------%

% -----------------------------------------------------------------------%
% --------- PARAMETERS --------------------------------------------------%
% -----------------------------------------------------------------------%
L = 4;
XUV_time_window = 3000;
% parameters for spectrogram selection
tau_start = -4000;
tau_end = 4000;
W_start = 0;
W_end = 30;

workers = 6;    % number of workers for parallel computation
                % put to 0 if no parallel compuation is wanted
                
% ----- ACDC section ----- %
iter_ACDC = 10;   % number of iterations using the ACDC algorithm
folderName = 'Example_files'; %folder where to load spectrogram, XUV and IR params
ws = ['Params_example.mat']; % .mat file from which XUV, IR and k0 will be loaded

params = load([folderName '/' ws], 'A', 't_A', 'k0', 'N_P', 'XUV_time_window', 't_XUV', 'E_XUV');
IR_params = struct('A', params.A, 't_A', params.t_A);
k0 = params.k0;
XUV_params = struct('N_P', params.N_P, 'XUV_time_window', params.XUV_time_window, 't_XUV', params.t_XUV, 'E_XUV', params.E_XUV);
disp(['Parameters of IR and XUV loaded!!!']) 
disp(['Central momentum enegy k0 = ' num2str(k0) ' eV'])

% load spectrogram
mom_normalization = 1; % apply momentum normalization of the spectrogram
change_ene_res = 1; % if you want to apply energy interpolation of the spectrogram
ene_res = 0.3; % if change_ene_res = 1 set the new energy resolution in [eV]

dataName = [folderName '/Spectrogram_example.mat'];
SpectData = load(dataName);
W=SpectData.W;
tau=SpectData.tau; % spectrum in as vs. eV
S=SpectData.S;

want_plot = 1; %of plot every 100 iters ACDC and GD

% ----- Gradient descent section ----- %
W_S = 0; %starting energy value
W_E = 25; %ending energy value

beta1 = 0.9;
beta2 = 0.999;
epsilon_gd = 10^(-8);

delta_phase = 0.001; %0.001;
delta_amp = 0.001; %0.001;
num_iter_GD = 10;

MAX_PHASE = 0.0576;
MAX_AMP = 0.8164; 
decreasing_factor = 0.95;

% ----- Saving parameters ----- %
ACDC_ws_name = ['ACDC_workspace.mat'];
GD_ws_name = ['GradientDescent_workspace.mat'];

figure()
pcolor(tau*1e-3,W,S);
set(gca,'FontSize',18)
shading flat
title('Original spectrogram')
xlabel('Delay (fs)')
ylabel('Energy (eV)')
colormap jet
colorbar


if change_ene_res
    del_W = W(2)-W(1);
    W_new = linspace(min(W)+0*del_W, max(W),(max(W)-min(W))/ene_res);
    tau_new = tau;
    y = interp2(tau, W', S,tau_new, W_new');
    
    figure()
    pcolor(tau_new*1e-3,W_new,y/max(max(y)));
    set(gca,'FontSize',18)
    shading flat
    title('Spectrogram after energy interpolation')
    xlabel('Delay (fs)')
    ylabel('Energy (eV)')
    colormap jet
    colorbar
    
    disp(['Energy interpolation of the spectrogram - actual energy resolution =  ' num2str(ene_res) ' eV'])
    % rename variables after interpolation
    tau = tau_new;
    W = W_new;
    S = y;
end

% start parallel section
if workers > 0 && isempty(gcp('nocreate'))
    myCluster=parcluster('local');
    myCluster.NumWorkers=workers;
    parpool(myCluster,workers)
end
%%
k0 = sqrt(2*evcon*k0); %conversion to atomic unit
%-----------------------------------------------------------%
%----------------Load setting parameters--------------------%
%-----------------------------------------------------------%
tau_range = tau*1e-18*tcon;
W_range = W*evcon;
S_range = abs(S);
%
del_t = (tau(2)-tau(1))/L; % in as

%-----------------------------------------------%
%----------- E_XUV initial parameters ----------%
%-----------------------------------------------%
% XUV_time_window
N_P = ceil(XUV_time_window/del_t);
N_P = N_P + mod(N_P, 2);

t_XUV = zeros(1, N_P);
count = 0;
for i=1:N_P
    t_XUV(count+1)=(count-N_P/2)*del_t;
    count=count+1;
end
if(isfield(XUV_params,{'N_P', 'XUV_time_window', 't_XUV', 'E_XUV'})) %I have initial guess of XUV and/or XUV1
    disp('XUV initial guess...')
    N_P_loaded = XUV_params.N_P;
    XUV_time_window_loaded = XUV_params.XUV_time_window;
    t_XUV_loaded = XUV_params.t_XUV/(1e-18*tcon);
    E_XUV_loaded = XUV_params.E_XUV;%/max(abs(XUVparams.E_XUV));

    if(N_P ~= N_P_loaded || XUV_time_window ~= XUV_time_window_loaded)
        t_axis = zeros(1, N_P);
        count = 0;
        for i=1:N_P
            t_axis(count+1)=(count-N_P/2)*del_t;
            count=count+1;
        end
        E_XUV_interp = interp1(t_XUV_loaded, E_XUV_loaded, t_axis, 'spline',0);
        E_XUV = E_XUV_interp;
        t_XUV = t_axis;
        disp('Interpolation of the XUV...Done!!!')
    else
        N_P = N_P_loaded;
        XUV_time_window = XUV_time_window_loaded;
        t_XUV = t_XUV_loaded;
        E_XUV = E_XUV_loaded;
    end
    disp('XUV parameters successfully loaded!!!')
else
    error('XUV parameters not found. Check variable XUV_params that contains the correct fields!')
end
%conversion to atomic unit
t_XUV = t_XUV*1e-18*tcon;
%%
del_t = del_t*1e-18*tcon;
max_range = (2*pi)/(evcon*del_t);
%--------- first trace ----------%
W_ind = find(W >= W_start & W <= W_end);

% rescale W_range
W_range = W_range(W_ind);
S_range = S_range(min(W_ind):max(W_ind),:);
S_range = abs(S_range);

% rescale tau_range
center_IR_load = (tau_end + tau_start)/2; 
tau_ind = find(tau >= tau_start & tau <= tau_end);
tau_range = tau_range(tau_ind);

% rescale S_range and S_range1
S_range = S_range(:,min(tau_ind):max(tau_ind));
%normalization of S_range and S_range1
S_range = S_range/max(max(abs(S_range)));

%-----------------------------------------------%
%-------------------- A Data -------------------%
%-----------------------------------------------%
%Load IR from SIM file
N_A = (size(tau_range, 2)-1)*L + N_P;
t_A = zeros(1, N_A);
count = 0;
for i=1:N_A
    t_A(i)=(count-double(N_A)/2)*del_t;
    count=count+1;
end


t_A_load = IR_params.t_A*tcon;
A_load = IR_params.A;

figure()
    plot(t_A_load, A_load)

center = ((tau_end + tau_start)/2)*1e-18*tcon;
center_IR_load = center_IR_load*1e-18*tcon;
A = interp1(t_A_load + center_IR_load - center, A_load, (t_A), 'spline');

disp('Interpolation of the IR field in the correct time window...Done!!!')
%%
% We need to set f_norm vector to appropriate size
f_norm = ones(1, size(S_range, 1));
% and energy filter
energy_filter = ones(1, size(S_range, 1));

%-----------------------------------------------%
%--------------- DTME information --------------%
%-----------------------------------------------%
% inizialization of DTME to unit vector
W_dipole = W_range;

real_dk = ones(1,length(W_range));
imag_dk = zeros(1,length(W_range));
g = real_dk + 1i*imag_dk;
dipole = true;
%%
%-----------------------------------------------%
%----------------- Final tasks -----------------%
%-----------------------------------------------%
k_vec = sqrt(2*W_range);
%for integration of error and normalization define delta_W vector
delta_W_vec = zeros(1, size(W_range, 2));
delta_W_vec(1) = W_range(2) - W_range(1);
for i=2:size(W_range,2)-1
    delta_W_vec(i) = (W_range(i+1) - W_range(i-1))/2;
end
delta_W_vec(end) = W_range(end) - W_range(end-1);
delta_W_vec_sum = sum(delta_W_vec, 2);

if mom_normalization == 1
    %Momentum normalization
    S_range = bsxfun(@rdivide, S_range, k_vec');
    S_range(isinf(S_range)|isnan(S_range)) = 0;
    disp('Momentum normalization of the spectrogram...Done!!!')
end

%normalization of S_range
S_range = S_range/max(max(abs(S_range)));
%%
% build action matrix
action_matrix = zeros(size(k_vec, 2),N_A);
for l=1:size(k_vec, 2)
    % variables for DTME (f_real/f_imag), the action term and calculation of the action integral
    integrand = k_vec(l)*A + (A.^2)/2;
    for k=1:N_A
        for j=k:N_A
            action_matrix(l,k) = action_matrix(l,k) + integrand(j);
        end
    end
    action_matrix(l,:) = del_t*action_matrix(l,:);
end
s = struct('N_P',N_P, 'k_vec',k_vec, 'A', A, 'del_t', del_t, 'dipole', dipole,...
    'W_range', W_range, 'E_XUV', E_XUV, 't_XUV', t_XUV, 'k0', k0, 'L', L,...
    'W_dipole', W_dipole, 'real_dk', real_dk, 'imag_dk', imag_dk, 'tau_range', tau_range, 'action_matrix', action_matrix);
S_prime = S_prime_calc(s);

% check normalization of initial guess to 1
E_XUV = E_XUV/max(max(abs(S_prime)));

%I calculate S_range using S_prime_calc
s = struct('N_P',N_P, 'k_vec',k_vec, 'A', A, 'del_t', del_t, 'dipole', dipole,...
    'W_range', W_range, 'E_XUV', E_XUV, 't_XUV', t_XUV, 'k0', k0, 'L', L,...
    'W_dipole', W_dipole, 'real_dk', real_dk, 'imag_dk', imag_dk, 'tau_range', tau_range, 'action_matrix', action_matrix);
S_prime = S_prime_calc(s);

%% Final plots before starting to iterate
current_error = calculate_error(S_range(4:end-3, :), ...
            S_prime(4:end-3, :), delta_W_vec(4:end-3), delta_W_vec_sum);
disp(['starting error = ' num2str(current_error)])

disp(['L= ' num2str(L)])
disp(['resulting in a sample width of ' num2str(del_t/(1e-18*tcon)) 'as.'])
disp(['XUV time window of ' num2str((t_XUV(end)-t_XUV(1))/(1e-18*tcon)) 'as.'])
disp(['XUV time window of ' num2str(N_P) ' points'])

figure()
ax1=subplot(1,3,1);
pcolor(tau_range/(1e-15*tcon),W_range'/evcon,S_range);
shading flat
title('Input spectrogram')
xlabel('Delay (fs)')
ylabel('Energy (eV)')
set(gca,'FontSize',20)
axis([min(tau_range/(1e-15*tcon))  max(tau_range/(1e-15*tcon)) min(W_range/evcon) max(W_range/evcon)])
colormap(ax1,'jet')
colorbar
%caxis([0 1])
ax2=subplot(1,3,2);
pcolor(tau_range/(1e-15*tcon),W_range'/evcon,(abs(S_prime)).^2);
shading flat
title('Output spectrogram')
xlabel('Delay (fs)')
ylabel('Energy (eV)')
set(gca,'FontSize',20)
axis([min(tau_range/(1e-15*tcon))  max(tau_range/(1e-15*tcon)) min(W_range/evcon) max(W_range/evcon)])
colormap(ax2,'jet')
colorbar
%caxis([0 1])
ax3=subplot(1,3,3);
pcolor(tau_range/(1e-15*tcon),W_range'/evcon,(abs(S_prime)).^2-(S_range));
shading flat
title('Output - Input')
xlabel('Delay (fs)')
ylabel('Energy (eV)')
set(gca,'FontSize',16)
axis([min(tau_range/(1e-15*tcon))  max(tau_range/(1e-15*tcon)) min(W_range/evcon) max(W_range/evcon)])
colormap(ax3,'jet')
colorbar
caxis([-0.2 0.2])


phT =angle(E_XUV);
abT =abs(E_XUV/max(abs(E_XUV)));
phT(abT<0.01)=NaN;
phT=unwrap(phT);
phT=phT-max(phT);

figure()
plot(t_XUV/(1e-18*tcon), abs(E_XUV).^2, 'b-x')
title('XUV Amplitude and phase')
xlabel('Time (as)')
ylabel('Normalized intensity (eV)')
hold on
xlim([-1500 1500])
set(gca,'FontSize',16)
yyaxis right
ylabel('Phase (rad)')
plot(t_XUV/(1e-18*tcon),unwrap(angle(E_XUV)),'g-x')
hold off

%%
%-----------------------------------------------%
%---------------- Start iterate ----------------%
%-----------------------------------------------%
action_exp = zeros(size(k_vec, 2), N_A);
for i=1:size(k_vec, 2)
    % variables for DTME (f_real/f_imag), the action term and calculation of the action integral
    integrand = k_vec(i)*A + (A.^2)/2;
    %cumsum_reverse
    action = zeros(1, N_A);
    for k=1:N_A
        action(k) = 0;
        for j=k:N_A
            action(k) = action(k) + integrand(j);
        end
    end
    action = del_t*action;
    action_exp(i, :) = exp(-1i*(action));
end

% initialize the variables to zero.
gamma_dtme = zeros(size(k_vec, 2), size(tau_range, 2));
beta_dtme = zeros(size(k_vec, 2), size(tau_range, 2));
epsilon = zeros(size(k_vec, 2), size(tau_range, 2));
delta = zeros(size(k_vec, 2), size(tau_range, 2));
eta = zeros(size(k_vec, 2), size(tau_range, 2));

for mm=1:size(k_vec, 2)
    for ll=1:size(tau_range, 2)
        gamma_dtme(mm,ll) = -1i*del_t*sum(E_XUV.*exp(1i*(W_range(mm) - (k0^2)/2)*t_XUV).*action_exp(mm, L*(ll-1) + 1 : N_P+L*(ll-1)));
        beta_dtme(mm,ll)  = -1i*del_t*sum(E_XUV.*exp(1i*(W_range(mm) - (k0^2)/2)*t_XUV).*action_exp(mm, L*(ll-1) + 1 : N_P+L*(ll-1)).*A(L*(ll-1) + 1 : N_P+L*(ll-1)));
    end
end

ERR_TRACK1 = zeros(1, iter_ACDC);
% start the watch to count the time
tic
error_1 = 1;
for y=1:(iter_ACDC)
    
    if(y==1)
        disp(['ACDC iterations started'])
    end
    if(y>=100 && mod(y,1)==0)
        disp(['iteration number: ' num2str(y) '/' num2str(iter_ACDC)])
    elseif (y<100)
        disp(['iteration number: ' num2str(y) '/' num2str(iter_ACDC)])
    end
    error_1 = current_error;

    %-----------------------------------------------------%
    %--------------- DTME calculation --------------------%
    %-----------------------------------------------------%
    starting_g = g;
    starting_error = error_1;
    reverse = false;
    for b=1:2
        if ~reverse
            START_ene = 1;
            END_ene = size(k_vec, 2);
            START_time = START_ene;
            END_time = size(tau_range, 2);
            step = 1;
        else
            START_ene = size(k_vec, 2);
            END_ene = 1;
            START_time = size(tau_range, 2);
            END_time = END_ene;
            step = -1;
        end
        
        % calculate S_prime via the formula (7)
        for mm=START_ene:step:END_ene
            for ll=START_time:step:END_time
                if(mm == 1)
                    S_prime(mm,ll) = 0;
                elseif(mm == size(k_vec, 2))
                    S_prime(mm,ll) = 0;
                else
                    S_prime(mm,ll) = g(mm)*gamma_dtme(mm,ll) + g(mm+1)*beta_dtme(mm,ll)/(k_vec(mm+1)-k_vec(mm-1)) - g(mm-1)*beta_dtme(mm,ll)/(k_vec(mm+1)-k_vec(mm-1));
                end
            end
        end
        
        error_1 = calculate_error(S_range(4:end-3, :), ...
            S_prime(4:end-3, :), delta_W_vec(4:end-3), delta_W_vec_sum);
        
        % perform the projection
        % project before beginning minimization
        S_prime = sqrt(abs(S_range)).*exp(1i*angle(S_prime)); % a' in the notes
        
        for mm=START_ene:step:END_ene
            for ll=START_time:step:END_time
                if(mm > 2 && mm < size(k_vec, 2)-2)
                    epsilon(mm,ll) = conj(g(mm-1))*conj(gamma_dtme(mm-1,ll))*(beta_dtme(mm-1,ll)/(k_vec(mm)-k_vec(mm-2)))...
                        - conj(g(mm-2))*(abs(beta_dtme(mm-1,ll))^2)/((k_vec(mm)-k_vec(mm-2))^2)...
                        - (beta_dtme(mm-1,ll))*conj(S_prime(mm-1,ll))/(k_vec(mm)-k_vec(mm-2)); % formula (18)
                    
                    delta(mm,ll) = gamma_dtme(mm,ll)*conj(g(mm+1))*conj(beta_dtme(mm,ll))/(k_vec(mm+1)-k_vec(mm-1))...
                        - gamma_dtme(mm,ll)*conj(g(mm-1))*conj(beta_dtme(mm,ll))/(k_vec(mm+1)-k_vec(mm-1))...
                        - gamma_dtme(mm,ll)*conj(S_prime(mm,ll)); % formula (19)
                    
                    eta(mm,ll) = -conj(g(mm+1))*conj(gamma_dtme(mm+1,ll))*(beta_dtme(mm+1,ll)/(k_vec(mm+2)-k_vec(mm)))...
                        - conj(g(mm+2))*(abs(beta_dtme(mm+1,ll))^2)/((k_vec(mm+2)-k_vec(mm))^2)...
                        + (beta_dtme(mm+1,ll))*conj(S_prime(mm+1,ll))/(k_vec(mm+2)-k_vec(mm)); % formula (20)
                end
            end
        end
        
        if(~reverse)
            for mm=START_ene+2:step:END_ene-2
                NUM = sum(conj(epsilon(mm,:)) + conj(delta(mm,:)) + conj(eta(mm,:)));
                DEN = sum(abs(gamma_dtme(mm,:)).^2) ...
                    + sum(abs(beta_dtme(mm+1,:)).^2)/((k_vec(mm+2)-k_vec(mm))^2) ...
                    + sum(abs(beta_dtme(mm-1,:)).^2)/((k_vec(mm)-k_vec(mm-2))^2);
                
                g(mm) = (-1)*NUM/DEN;
            end
        else
            for mm=START_ene-2:step:END_ene+2
                NUM = sum(conj(epsilon(mm,:)) + conj(delta(mm,:)) + conj(eta(mm,:)));
                DEN = sum(abs(gamma_dtme(mm,:)).^2) ...
                    + sum(abs(beta_dtme(mm+1,:)).^2)/((k_vec(mm+2)-k_vec(mm))^2) ...
                    + sum(abs(beta_dtme(mm-1,:)).^2)/((k_vec(mm)-k_vec(mm-2))^2);
                
                g(mm) = (-1)*NUM/DEN;
            end
        end
        
        real_dk = real(g);
        imag_dk = imag(g);
        
        type = 'linear';
        edge_points = 0;
        F_real = griddedInterpolant(k_vec((edge_points+1):end-edge_points),real_dk((edge_points+1):end-edge_points), ...
            'linear', type);
        REAL_int = F_real(k_vec);
        real_dk(1:edge_points) = REAL_int(1:edge_points);
        real_dk(end-edge_points:end) = REAL_int(end-edge_points:end);
        F_imag = griddedInterpolant(k_vec((edge_points+1):end-edge_points),imag_dk((edge_points+1):end-edge_points), ...
            'linear', type);
        IMAG_int = F_imag(k_vec);
        imag_dk(1:edge_points) = IMAG_int(1:edge_points);
        imag_dk(end-edge_points:end) = IMAG_int(end-edge_points:end);
        
        g = complex(real_dk, imag_dk);
        
        %--------------------------------
        % calculate S_prime
        %--------------------------------
        % update S_prime
        s = struct('N_P',N_P, 'k_vec',k_vec, 'A', A, 'del_t', del_t, 'dipole', dipole,...
                'W_range', W_range, 'E_XUV', E_XUV, 't_XUV', t_XUV, 'k0', k0, 'L', L,...
                'W_dipole', W_dipole, 'real_dk', real_dk, 'imag_dk', imag_dk, 'tau_range',...
                tau_range, 'action_matrix', action_matrix);
        S_prime = S_prime_calc(s);
        
        current_error = calculate_error(S_range(4:end-3, :), ...
            S_prime(4:end-3, :), delta_W_vec(4:end-3), delta_W_vec_sum);
        
        if(mod(y,100)==0)
            disp(['current error: ' num2str(error_1)])
        end
        
        reverse = ~reverse;
    end
 
    if want_plot || mod(y,100)==0
        z_dipole = complex(real_dk, imag_dk);
        W_dip = W_dipole/evcon;
        d_ene = W_dip(2)-W_dip(1);
        grad = ((1e18*h_bar)/el)*gradient(unwrap(angle(z_dipole)),d_ene); % GD_Ar in as
        
        
        figure(316)
        plot(W_dipole/evcon, grad, 'bx-','DisplayName', 'Reconstructed','linewidth', 2)
        xlabel('Electron energy (eV)')
        ylabel('GD (as)')
        hold on
        xlim([0 45])
        ylim([-200 300])
        set(gca,'FontSize',18)
        hold off
        drawnow
    end

    ERR_TRACK1(y) = error_1;
    
    if(y>1 && ERR_TRACK1(y-1)<ERR_TRACK1(y))
        disp('error increasing')
        save('LOWEST_ERROR_workspace.mat','-v7.3')
    end
    
end
save(ACDC_ws_name,'-v7.3')
disp('ACDC finished...and workspace saved!!!')
elapsed_time = toc;
disp(['Elapsed time = ' num2str(elapsed_time)])

%%
disp('Starting Gradient Descent algorithm')
%-----------------------------------------------%
%-------------- GRADIENT DESCENT ---------------%
%-----------------------------------------------%
%start parallel loop if not alredy running
if isempty(gcp('nocreate'))
    myCluster=parcluster('local');
    myCluster.NumWorkers=workers;
    pool = parpool(myCluster,workers);
end

%initialize g vector
g_orig = g;

real_dk_orig = real(g_orig);
imag_dk_orig = imag(g_orig);
W_dipole_orig = W_dipole;

index = find(W_range'/evcon >= W_S & W_range'/evcon <= W_E);
%rescaling the relevant variables
W_range = (W_range(index));
S_range = (S_range(index,:));
S_prime = (S_prime(index,:));
k_vec = (k_vec(index));
f_norm = (f_norm(index));
W_dipole = (W_dipole(index));
g = g(index);
%g = rand(1,length(index)).*exp(1i*rand(1,length(index)));
%g = rand(1,length(index)).*exp(1i*rand(1,length(index)));
%g = smoothdata(g,'gaussian');
real_dk = (real(g));
imag_dk = (imag(g));
energy_filter = (energy_filter(index));

%Adam parameters
V_dW = zeros(1,length(g));
S_dW = zeros(1,length(g));

x = 1:1:num_iter_GD;
cost_func = zeros(1,num_iter_GD);

gamma_max_phase = ones(1, length(W_dipole))*MAX_PHASE;
gamma_max_amp = ones(1, length(W_dipole))*MAX_AMP;

s_test = struct('N_P',N_P, 'k_vec',k_vec, 'A', A, 'del_t', del_t, 'dipole', dipole,...
    'W_range', W_range, 'E_XUV', E_XUV, 't_XUV', t_XUV, 'k0', k0, 'L', L,...
    'W_dipole', W_dipole, 'real_dk', real_dk, 'imag_dk', imag_dk, 'tau_range', tau_range, 'action_matrix', action_matrix);
S_prime_prime = S_prime_calc(s_test);

% calculate the initial error -> it will be the current_error for the first iteration
idx = find(W_dipole/evcon>=W_S & W_dipole/evcon<=W_E);

W_range_err = W_range(idx);
S_range_err = S_range(idx,:);
% update delta_W_vec
delta_W_vec_err = zeros(1, size(W_range_err, 2));
delta_W_vec_err(1) = W_range_err(2) - W_range_err(1);
for i=2:size(W_range_err,2)-1
    delta_W_vec_err(i) = (W_range_err(i+1) - W_range_err(i-1))/2;
end
delta_W_vec_err(end) = W_range_err(end) - W_range_err(end-1);
delta_W_vec_sum_err = sum(delta_W_vec_err, 2);

current_error = ones(1, length(W_dipole));

delta_W_vec_err_matrix = repmat(delta_W_vec_err', 1, size(S_range_err, 2));

c_err = calculate_error(S_range_err, ...
    S_prime_prime(idx,:), delta_W_vec_err_matrix, delta_W_vec_sum_err);

current_error = current_error*c_err;

START = idx(1);
END = idx(end);

N_P_c = parallel.pool.Constant(N_P);
del_t_c = parallel.pool.Constant(del_t);
k0_c = parallel.pool.Constant(k0);
L_c = parallel.pool.Constant(L);
epsilon_c = parallel.pool.Constant(epsilon_gd);
delta_W_vec_sum_err_c = parallel.pool.Constant(delta_W_vec_sum_err);

for i=1:num_iter_GD
    
    g_start = repmat(g,length(g),1);
    if reverse
        len = lenght(idx) + 1;
    else
        len = 0;
    end

    parfor mm=START:1:END
        % ---------------------------- %
        % -----------PHASE------------ %
        % ---------------------------- %
        % set the gamma value at point m
        gamma_phase = gamma_max_phase(mm);
        % ---------------------------- %
        % Update the phase at point mm
        g_init = g_start(mm,:);
        g_loop = g_start(mm,:);
        ang = unwrap(angle(g_loop(mm)));
        g_loop(mm) = abs(g_loop(mm))*exp(1i*(ang + delta_phase));
        
        % ---------------------------- %
        % calculate S_prime
        s_loop = struct('N_P',N_P_c.Value, 'k_vec',k_vec, 'A', A, 'del_t', del_t_c.Value,...
            'W_range', W_range, 'E_XUV', E_XUV, 't_XUV', t_XUV, 'k0', k0_c.Value, 'L', L_c.Value,...
            'W_dipole', W_dipole, 'real_dk', real(g_loop), 'imag_dk', imag(g_loop),...
            'tau_range', tau_range, 'action_matrix', action_matrix);

        S_prime_new = S_prime_calc(s_loop);

        % ---------------------------- %
        %compute the error (here limited S_range!!! -> check without limitation)
        err = calculate_error(S_range_err, ...
            S_prime_new(idx,:), delta_W_vec_err_matrix, delta_W_vec_sum_err_c.Value);
        % ---------------------------- %
        %recording the error at each energy point & calculating the
        %gradient
        grad_err = (err - current_error(mm))/(delta_phase);

        %apply Adam
        V_dW(mm) = beta1 + (1-beta1)*grad_err;
        S_dW(mm) = beta2 + (1-beta2)*(grad_err)^i;
        V_dW_corr = V_dW(mm)/(1-beta1^i);
        S_dW_corr = S_dW(mm)/(1-beta2^i);

        g_loop(mm) = abs(g_loop(mm))*exp(1i*(ang - gamma_phase*(V_dW_corr/(sqrt(S_dW_corr) + epsilon_c.Value)))); % Adam version

        % ---------------------------- %
        % calculate new spectrogram
        s_loop.real_dk(mm) = real(g_loop(mm));
        s_loop.imag_dk(mm) = imag(g_loop(mm));
        S_prime_final = S_prime_calc(s_loop);
        %compute the error
        err_final = calculate_error(S_range_err, ...
            S_prime_final(idx,:), delta_W_vec_err_matrix, delta_W_vec_sum_err_c.Value);
        % ---------------------------- %
        c_err_phase = err_final;

        if (err_final > current_error(mm)) % the error diverges -> reduce gamma at this point
            gamma_max_phase(mm) = gamma_max_phase(mm)*(decreasing_factor);
            g_loop(mm) = abs(g_init(mm))*exp(1i*(ang));
            c_err_phase = current_error(mm);
        end
        g_start(mm,:) = g_loop;
        
        % -------------------------------- %
        % -----------AMPLITUDE------------ %
        % -------------------------------- %
        gamma_amp = gamma_max_amp(mm);
        % Update the amplitude at point mm
        g_init = g_start(mm,:);
        g_loop = g_start(mm,:);
        amp = abs(g_loop(mm));
        g_loop(mm) = (amp - delta_amp)*exp(1i*(unwrap(angle(g_loop(mm)))));
        
        % ---------------------------- %
        % calculate S_prime
        s_loop.real_dk(mm) = real(g_loop(mm));
        s_loop.imag_dk(mm) = imag(g_loop(mm));
        S_prime_new = S_prime_calc(s_loop);        
        % ---------------------------- %
        %compute the error (here limited S_range!!! -> check without limitation)
        err = calculate_error(S_range_err, ...
            S_prime_new(idx,:), delta_W_vec_err_matrix, delta_W_vec_sum_err_c.Value);
        % ---------------------------- %
        %recording the error at each energy point & calculating the
        %gradient
        grad_err = (err - c_err_phase)/(delta_amp);

        g_loop(mm) = (amp + gamma_amp*grad_err)*exp(1i*(unwrap(angle(g_loop(mm)))));
        % ---------------------------- %
        % calculate new spectrogram
        s_loop.real_dk(mm) = real(g_loop(mm));
        s_loop.imag_dk(mm) = imag(g_loop(mm));
        S_prime_final = S_prime_calc(s_loop);
        %compute the error
        err_final = calculate_error(S_range_err, ...
            S_prime_final(idx,:), delta_W_vec_err_matrix, delta_W_vec_sum_err_c.Value);
        % ---------------------------- %
        if (err_final > c_err_phase) % the error diverges -> reduce gamma at this point
            gamma_max_amp(mm) = gamma_max_amp(mm)*(decreasing_factor);
            g_loop(mm) = amp*exp(1i*(unwrap(angle(g_init(mm)))));
        end
        g_start(mm,:) = g_loop;
    end

    g = diag(g_start).';
    s_test.real_dk = real(g);
    s_test.imag_dk = imag(g);
    S_prime_prime = S_prime_calc(s_test);
    c_err = calculate_error(S_range(idx,:), ...
        S_prime_prime(idx,:), delta_W_vec_err_matrix, delta_W_vec_sum_err);
    current_error = ones(1, length(W_dipole))*c_err;
    cost_func(i) = c_err;
    
    if i<=10 || mod(i,100)==0
        disp(['iter number ' num2str(i) ' - current error: ' num2str(c_err)])
    end
    if want_plot && mod(i,100)==0
        
        figure(100)
        loglog(x(1:i), cost_func(1:i), 'o-g')
        title('Cost function')
        xlabel('Iteration #')
        ylabel('Error')
        set(gca,'FontSize',20)
        grid on
        drawnow
        
        ax_lim = max(max(abs((abs(S_prime_prime)).^2-(S_range))));
        ax3=figure(101);
        pcolor(tau_range/(1e-15*tcon),W_range'/evcon,(abs(S_prime_prime)).^2-(S_range));
        shading flat
        title('Out - In')
        xlabel('Delay (fs)')
        ylabel('Energy (eV)')
        set(gca,'FontSize',20)
        axis([min(tau_range/(1e-15*tcon))  max(tau_range/(1e-15*tcon)) (W_range(START)/evcon) (W_range(END)/evcon)])
        colormap jet
        colorbar
        caxis([-ax_lim ax_lim])
        
        figure(102)
        plot(W_dipole(idx)/evcon, abs(g(idx)), 'kx-','DisplayName', 'Reconstructed GD','linewidth', 1)
        hold on
        title('DTME - amplitude and phase')
        xlabel('Electron energy (eV)')
        ylabel('abs(DTME) (au)')
        yyaxis right
        ylabel('phase(DTME) (rad)')
        plot(W_dipole(idx)/evcon, unwrap(angle(g(idx))), 'bx-','DisplayName', 'Reconstructed GD','linewidth', 1)
        xlim([W_S W_E])
        set(gca,'FontSize',20)
        hold off
        %legend('show')
    end
    
    if (i == num_iter_GD)
        save(GD_ws_name)
        disp('Gradient descent finished...and workspace saved!!!')
    end
    
end

